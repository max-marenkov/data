<?php

abstract class Mem
{
    protected $name;

    public function __construct()
    {
        $this->getName();

        $this->printName();
    }

    private function getName()
    {
        $this->name = get_called_class();
    }

    protected function printName()
    {
        echo $this->name . '<br />';
    }
}